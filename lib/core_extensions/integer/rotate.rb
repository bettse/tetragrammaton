module CoreExtensions
  module Integer
    module Rotate
      def ror(count)
        if (count == 32)
          return self
        else
          (self >> count) | (self << (32 - count)) & MASK32
        end
      end
    end
  end
end
Integer.include CoreExtensions::Integer::Rotate
