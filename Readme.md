# Tetragrammaton

A simple service to calculate a four byte password for a given 7 byte UID

##Examples:

### Curl

`curl --data "uid=0462b68ab44280" http://localhost:4567`

### Httpie

`http -f POST http://localhost:4567 uid=0462b68ab44280`
