HEX = 0x10
MASK32 = 0xFFFFFFFF

class PwdController < ApplicationController
  protect_from_forgery with: :null_session
  skip_before_filter  :verify_authenticity_token
  newrelic_ignore only: [:index]

  def index
    respond_to do |format|
      format.all { render nothing: true, status: 200 }
    end
  end

  def new
    respond_to do |format|
      format.html { render }
    end
  end

  def create
    uid = params['uid']
    if (uid && uid.length == 14)
      pwd = sprintf("%08X", (uid2pwd(uid) & MASK32)) # Masking to remove sign bit
      respond_to do |format|
        format.text { render text: revHex(pwd) }
        format.html { render html: revHex(pwd) }
        format.json { render json: {pwd: revHex(pwd) } }
      end
    else
      render nothing: true, status: :bad_request
    end
  end

  private
  def revHex(s)
    [s].pack('H*').unpack('N*').pack('V*').unpack('H*').first
  end

  def uid2pwd(uid)
    input = [
      revHex(uid[0..7]).to_i(HEX),
      revHex(uid[8..14] + '28').to_i(HEX),
      0x43202963,
      0x7279706f,
      0x74686769,
      0x47454c20,
      0x3032204f,
      0xaaaa3431
    ]
    magic_hash(input)
  end

  def magic_hash(input)
    input.reduce(0) do |result, value|
      a = result.ror(25)
      b = result.ror(10)
      (value + a + b - result) & MASK32
    end
  end
end
