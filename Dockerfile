FROM ruby:2.2

# throw errors if Gemfile has been modified since Gemfile.lock
RUN bundle config --global frozen 1

RUN apt-get update && apt-get install -y nodejs --no-install-recommends && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
ENV RAILS_ENV production

COPY Gemfile /usr/src/app/
COPY Gemfile.lock /usr/src/app/
RUN bundle install

COPY . /usr/src/app

RUN RAILS_ENV=production bundle exec rake assets:precompile
RUN echo "America/Los_Angeles" > /etc/timezone &&  dpkg-reconfigure -f noninteractive tzdata

EXPOSE 7668

CMD ["rails", "server", "-b", "0.0.0.0", "-p", "7668"]
